# Crea la VPC y las subredes
resource "aws_vpc" "vpcAPPTecnica" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
}

resource "aws_subnet" "subnet1" {
  vpc_id            = aws_vpc.vpcAPPTecnica.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"
}

resource "aws_internet_gateway" "igw_app" {
  vpc_id = aws_vpc.vpcAPPTecnica.id
}

resource "aws_subnet" "subnet2" {
  vpc_id            = aws_vpc.vpcAPPTecnica.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"
}
# Route Tables
resource "aws_route_table" "tab_ruta" {
  vpc_id = aws_vpc.vpcAPPTecnica.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_app.id
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.tab_ruta.id
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.subnet2.id
  route_table_id = aws_route_table.tab_ruta.id
}

#Crea el Security group

resource "aws_security_group" "sgTecnica" {
  name        = "sgTecnica"
  description = "Permite Trafico"
  vpc_id      = aws_vpc.vpcAPPTecnica.id

  ingress {
    description = "Trafico entrada from VPC"
    from_port   = 0
    to_port     = 65535
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Crea el Load Balancer
resource "aws_lb" "app_load_balancer" {
  name               = "app-load-balancer-AppTecnica"
  load_balancer_type = "application"
  subnets            = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
}

# Crea la escucha del Load Balancer
resource "aws_lb_listener" "listener_lb_Tecnica" {
  load_balancer_arn = aws_lb.app_load_balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "ERROR"
      status_code  = "404"
    }
  }
}

# Crea el grupo de destino del Load Balancer
resource "aws_lb_target_group" "tg_AppTecnica" {
  name        = "tg-appTecnica"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.vpcAPPTecnica.id
  target_type = "ip"
}

# Crea una regla de escucha del Load Balancer
resource "aws_lb_listener_rule" "listener_AppTecnica" {
  listener_arn = aws_lb_listener.listener_lb_Tecnica.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg_AppTecnica.arn
  }

  condition {
    http_request_method {
      values = [
        "POST",
      ]
    }
  }
}


#Outputs
output "id_VPC" {
  value = aws_vpc.vpcAPPTecnica.id
}

output "lb_url" {
  value = aws_lb.app_load_balancer.dns_name
}
